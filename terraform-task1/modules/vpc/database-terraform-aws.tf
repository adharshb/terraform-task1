resource "aws_db_instance" "default" {
  allocated_storage    = 10
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t3.micro"
  db_name                 = "mydb"
  username             = "user"
  password             = "password"
  parameter_group_name = "default.mysql5.7"
  skip_final_snapshot  = true
  db_subnet_group_name = "${aws_db_subnet_group.db-subnet.name}"
}

resource "aws_db_subnet_group" "db-subnet" {
  name="db-r"
  subnet_ids = ["${aws_subnet.dev-subnet-private[0].id}","${aws_subnet.dev-subnet-private[1].id}"]

  tags = {
    Name = "My DB subnet group"
  }
}