resource "aws_internet_gateway" "dev-igw" {
  vpc_id = aws_vpc.dev-vpc.id
  tags = {
    Name = "dev-igw"
  }
}

resource "aws_route_table" "dev-public-crt" {
  vpc_id = aws_vpc.dev-vpc.id

  route {
    cidr_block = "0.0.0.0/0"  //associated subnet can reach everywhere
    gateway_id = aws_internet_gateway.dev-igw.id //CRT uses this IGW to reach internet
  }

  tags = {
    Name = "dev-public-crt"
  }
}
resource "aws_route_table" "dev-private-crt" {
  vpc_id = aws_vpc.dev-vpc.id
  tags = {
    Name = "dev-private-crt"
  }
}



resource "aws_route_table_association" "public" {
  count = length(var.subnet_cidrs_public)

  subnet_id      = element(aws_subnet.dev-subnet-public.*.id, count.index)
  route_table_id = aws_route_table.dev-public-crt.id
}

resource "aws_route_table_association" "private" {
  count = length(var.subnet_cidrs_public)

  subnet_id      = element(aws_subnet.dev-subnet-private.*.id, count.index)
  route_table_id = aws_route_table.dev-private-crt.id
}

# resource "aws_main_route_table_association" "vpc-route" {
#   vpc_id="${aws_vpc.dev-vpc.id}"
#   route_table_id = aws_route_table.dev-public-crt.id
# }


